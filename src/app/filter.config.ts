export const FILTER_CONFIG = {
    groups: [
        {
            title: 'label.data.operation',
            fields: [
                {
                    getMethod: ' /api/operation/version/suggestion?attributeName=operationCode',
                    title: 'label.operation.code',
                    attributeName: 'operationCode'
                },
                {
                    getMethod: ' /api/operation/version/suggestion?attributeName=projectName',
                    title: 'label.project.name',
                    attributeName: 'projectName'
                },
            ],
        },
        {
            title: 'label.data.version',
            fields: [
                {
                    getMethod: ' /api/operation/version/suggestion?attributeName=populationCode',
                    title: 'label.population.code',
                    attributeName: 'populationCode'
                },
            ],
        }
    ],
    minimumInputChars: 2,
    localizations: {
        title: 'filter.title',
        placeholder: 'filter.placeholder',
        conditionTitle: 'filter.condition.title',
        selectedConditionTitle: 'filter.selected-condition.title',
        back: 'filter.back',
        enterKey: 'filter.enterKey',
        noResults: 'filter.noResults',
        noSuggest: 'filter.noSuggest'
    }
};
