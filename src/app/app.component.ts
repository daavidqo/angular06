import {Component} from '@angular/core';
import {FILTER_CONFIG} from './filter.config';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'angular06-v8';
    readonly FILTER_CONFIG = FILTER_CONFIG;
}
