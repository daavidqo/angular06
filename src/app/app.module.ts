import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TsFilterModule} from '@posam/ts-filter';

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        TsFilterModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
